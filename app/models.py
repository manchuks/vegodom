from django.db import models
from django.utils import timezone
from django.contrib.sessions.models import Session


class Anketa(models.Model):
    datetime = models.DateField('Дата', default=timezone.now)
    telefon = models.CharField('Телефон', max_length=255)
    session = models.ForeignKey(Session, related_name='session_form', on_delete=models.SET_NULL, blank=True, null=True)

    class Meta:
        verbose_name = ('Anketa')
        verbose_name_plural = ('Обратный звонок')


    def __str__(self):
        return self.name


class Block1(models.Model):
    title = models.CharField('Заголовок', max_length=255, blank=True, null=True)
    baner = models.ImageField('Банер БОЛЬШОЙ', blank=True, null=True)
    text = models.TextField('Текст', max_length=5555)
    flag = models.CharField('Флажок', max_length=10)
    id = models.AutoField(primary_key=True)

    class Meta:
        verbose_name = ('Block1')
        verbose_name_plural = ('Акция большая')


    def __str__(self):
        return self.title


# class Usluga(models.Model):
#     title = models.CharField('Заголовок', max_length=255, blank=True, null=True)
#     baner = models.ImageField('Банер БОЛЬШОЙ', blank=True, null=True)
#     text = models.TextField('Полное описание', max_length=955155)
#
#     class Meta:
#         verbose_name = ('Usluga')
#         verbose_name_plural = ('Страница услуги')
#
#
#     def __str__(self):
#         return self.title


class Block2(models.Model):
    title = models.CharField('Заголовок', max_length=255, blank=True, null=True)
    baner = models.ImageField('Банер маленький', blank=True, null=True)
    text = models.TextField('Текст', max_length=555)

    class Meta:
        verbose_name = ('Block2')
        verbose_name_plural = ('Акция маленькая')


    def __str__(self):
        return self.title


class Adresses(models.Model):
    phone1 = models.CharField('Телефон1', max_length=255, blank=True, null=True)
    phone2 = models.CharField('Телефон2', max_length=255, blank=True, null=True)
    ulitsa = models.CharField('Адрес', max_length=555)
    email = models.EmailField('Почта', max_length=555)
    ssylka = models.CharField('Ссылка на сайт', max_length=555)

    class Meta:
        verbose_name = ('Adresses')
        verbose_name_plural = ('Контакты')


    def __str__(self):
        return self.phone1


class Price(models.Model):
    price = models.FileField('Прайс ', blank=True, null=True)

    class Meta:
        verbose_name = ('Price')
        verbose_name_plural = ('Прайс')



class Certificate(models.Model):
    title = models.CharField('Название сертификата', max_length=255, blank=True, null=True)
    cert = models.ImageField('Сертификат', blank=True, null=True)

    class Meta:
        verbose_name = ('Certificate')
        verbose_name_plural = ('Сертификаты')

    def __str__(self):
        return self.title
