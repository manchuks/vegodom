from django import forms
from django.forms import TextInput

from .models import Anketa


class AnketaForm(forms.ModelForm):
    class Meta:
        model = Anketa
        fields = ('telefon',)
        widgets = {
            'telefon': TextInput(attrs={'placeholder': 'Номер телефона'}),
        }
