from django.shortcuts import render, redirect
from .models import Block1, Block2, Adresses, Price, Certificate, Anketa
from django.template.loader import render_to_string
from django.contrib.sessions.models import Session
from .forms import AnketaForm
from django.core.mail import send_mail


def otkosdef(request):
    adress = Adresses.objects.all()
    cenas = Price.objects.all()
    context = dict(adress=adress, cenas=cenas)
    order = session_get(request)
    context['order'] = order

    if request.method == 'POST':
        form = AnketaForm(request.POST, instance=order)
        if form.is_valid():
            form.save()
            order.save()
            context['result'] = True

            html = render_to_string('email.html', {'order': order})
            # if not settings.DEBUG:
            send_mail(
                'Анкета с сайта',
                html,
                'kronos173@mail.ru',
                ['kronos173@mail.ru'],
                fail_silently=False,
            )
    context['form'] = AnketaForm(instance=order)
    return render(request, 'otkos.html', context)


def steklopaketdef(request):
    adress = Adresses.objects.all()
    cenas = Price.objects.all()
    context = dict(adress=adress, cenas=cenas)
    order = session_get(request)
    context['order'] = order

    if request.method == 'POST':
        form = AnketaForm(request.POST, instance=order)
        if form.is_valid():
            form.save()
            order.save()
            context['result'] = True

            html = render_to_string('email.html', {'order': order})
            # if not settings.DEBUG:
            send_mail(
                'Анкета с сайта',
                html,
                'kronos173@mail.ru',
                ['kronos173@mail.ru'],
                fail_silently=False,
            )
    context['form'] = AnketaForm(instance=order)

    return render(request, 'steklopaket.html', context)


def moskitdef(request):
    adress = Adresses.objects.all()
    cenas = Price.objects.all()
    context = dict(adress=adress, cenas=cenas)
    order = session_get(request)
    context['order'] = order

    if request.method == 'POST':
        form = AnketaForm(request.POST, instance=order)
        if form.is_valid():
            form.save()
            order.save()
            context['result'] = True

            html = render_to_string('email.html', {'order': order})
            # if not settings.DEBUG:
            send_mail(
                'Анкета с сайта',
                html,
                'kronos173@mail.ru',
                ['kronos173@mail.ru'],
                fail_silently=False,
            )
    context['form'] = AnketaForm(instance=order)

    return render(request, 'moskit.html', context)


def obslugadef(request):
    adress = Adresses.objects.all()
    cenas = Price.objects.all()
    context = dict(adress=adress, cenas=cenas)
    order = session_get(request)
    context['order'] = order

    if request.method == 'POST':
        form = AnketaForm(request.POST, instance=order)
        if form.is_valid():
            form.save()
            order.save()
            context['result'] = True

            html = render_to_string('email.html', {'order': order})
            # if not settings.DEBUG:
            send_mail(
                'Анкета с сайта',
                html,
                'kronos173@mail.ru',
                ['kronos173@mail.ru'],
                fail_silently=False,
            )
    context['form'] = AnketaForm(instance=order)

    return render(request, 'obsluga.html', context)


def podokonnikdef(request):
    adress = Adresses.objects.all()
    cenas = Price.objects.all()
    context = dict(adress=adress, cenas=cenas)
    order = session_get(request)
    context['order'] = order

    if request.method == 'POST':
        form = AnketaForm(request.POST, instance=order)
        if form.is_valid():
            form.save()
            order.save()
            context['result'] = True

            html = render_to_string('email.html', {'order': order})
            # if not settings.DEBUG:
            send_mail(
                'Анкета с сайта',
                html,
                'kronos173@mail.ru',
                ['kronos173@mail.ru'],
                fail_silently=False,
            )
    context['form'] = AnketaForm(instance=order)

    return render(request, 'podokonnik.html', context)


def helpdef(request):
    adress = Adresses.objects.all()
    cenas = Price.objects.all()
    context = dict(adress=adress, cenas=cenas)
    order = session_get(request)
    context['order'] = order

    if request.method == 'POST':
        form = AnketaForm(request.POST, instance=order)
        if form.is_valid():
            form.save()
            order.save()
            context['result'] = True

            html = render_to_string('email.html', {'order': order})
            # if not settings.DEBUG:
            send_mail(
                'Анкета с сайта',
                html,
                'kronos173@mail.ru',
                ['kronos173@mail.ru'],
                fail_silently=False,
            )
    context['form'] = AnketaForm(instance=order)

    return render(request, 'help.html', context)


def aboutdef(request):
    adress = Adresses.objects.all()
    cenas = Price.objects.all()
    certs = Certificate.objects.all()
    context = dict(adress=adress, cenas=cenas, certs=certs)
    order = session_get(request)
    context['order'] = order

    if request.method == 'POST':
        form = AnketaForm(request.POST, instance=order)
        if form.is_valid():
            form.save()
            order.save()
            context['result'] = True

            html = render_to_string('email.html', {'order': order})
            # if not settings.DEBUG:
            send_mail(
                'Анкета с сайта',
                html,
                'kronos173@mail.ru',
                ['kronos173@mail.ru'],
                fail_silently=False,
            )
    context['form'] = AnketaForm(instance=order)

    return render(request, 'about.html', context)


def index(request):
    blocks = Block1.objects.all()
    cenas = Price.objects.all()
    second_blocks = Block2.objects.all()
    adress = Adresses.objects.all()
    context = dict(blocks=blocks, second_blocks=second_blocks, adress=adress, cenas=cenas)
    # context = {}
    order = session_get(request)
    context['order'] = order

    if request.method == 'POST':
        form = AnketaForm(request.POST, instance=order)
        if form.is_valid():
            form.save()
            order.save()
            context['result'] = True

            html = render_to_string('email.html', {'order': order})
            # if not settings.DEBUG:
            send_mail(
                'Анкета с сайта',
                html,
                'kronos173@mail.ru',
                ['kronos173@mail.ru'],
                fail_silently=False,
            )
    context['form'] = AnketaForm(instance=order)
    return render(request, 'index.html', context)

def test(request):
    blocks = Block1.objects.all()
    cenas = Price.objects.all()
    second_blocks = Block2.objects.all()
    adress = Adresses.objects.all()
    context = dict(blocks=blocks, second_blocks=second_blocks, adress=adress, cenas=cenas)
    # context = {}
    order = session_get(request)
    context['order'] = order

    if request.method == 'POST':
        form = AnketaForm(request.POST, instance=order)
        if form.is_valid():
            form.save()
            order.save()
            context['result'] = True

            html = render_to_string('email.html', {'order': order})
            # if not settings.DEBUG:
            send_mail(
                'Анкета с сайта',
                html,
                'kronos173@mail.ru',
                ['kronos173@mail.ru'],
                fail_silently=False,
            )
    context['form'] = AnketaForm(instance=order)
    return render(request, 'test.html', context)


def session_get(request):
    if Anketa.objects.filter(session__session_key=request.session.session_key):
        return Anketa.objects.filter(session__session_key=request.session.session_key).first()
    else:
        order = Anketa()
        session = Session.objects.get(session_key=request.session.session_key)
        order.session = session
        order.save()
        return order
