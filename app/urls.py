from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('otkos', views.otkosdef),
    path('moskit', views.moskitdef),
    path('steklopaket', views.steklopaketdef),
    path('obsluga', views.obslugadef),
    path('podokonnik', views.podokonnikdef),
    path('help', views.helpdef),
    path('about', views.aboutdef),
    path('test', views.test),
]