from django.contrib import admin
from .models import Block1, Block2, Adresses, Price, Certificate,Anketa


@admin.register(Block1)
class Block1Admin(admin.ModelAdmin):
    list_display = ['title', 'baner', 'text', 'id']


# @admin.register(Usluga)
# class UslugaAdmin(admin.ModelAdmin):
#     list_display = ['title', 'baner', 'text', 'id']


@admin.register(Block2)
class Block2Admin(admin.ModelAdmin):
    list_display = ['title', 'baner', 'text', ]


@admin.register(Anketa)
class AnketaAdmin(admin.ModelAdmin):
    list_display = ['datetime', 'telefon', ]


@admin.register(Adresses)
class AdressesAdmin(admin.ModelAdmin):
    list_display = ['phone1', 'phone2', 'ulitsa', 'email', 'ssylka', ]


@admin.register(Price)
class Price1Admin(admin.ModelAdmin):
    list_display = ['price', ]


@admin.register(Certificate)
class Price1Admin(admin.ModelAdmin):
    list_display = ['title', 'cert', ]
