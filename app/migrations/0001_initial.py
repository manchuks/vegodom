# Generated by Django 3.1.3 on 2020-11-12 09:24

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('sessions', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Anketa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('datetime', models.DateField(default=django.utils.timezone.now, verbose_name='Дата')),
                ('fio', models.CharField(blank=True, max_length=255, null=True, verbose_name='ФИО')),
                ('telefon', models.CharField(max_length=255, verbose_name='Телефон')),
                ('email', models.EmailField(max_length=255, verbose_name='email')),
                ('vopros', models.TextField(max_length=555, verbose_name='Животные')),
                ('massage', models.TextField(max_length=555, verbose_name='Сообщение')),
                ('slug', models.SlugField(max_length=255, verbose_name='Kategory')),
                ('session', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='session_form', to='sessions.session')),
            ],
        ),
    ]
