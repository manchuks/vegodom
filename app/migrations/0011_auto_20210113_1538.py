# Generated by Django 3.1.3 on 2021-01-13 11:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0010_remove_anketa_time'),
    ]

    operations = [
        migrations.AddField(
            model_name='block1',
            name='text2',
            field=models.CharField(default=1, max_length=95555, verbose_name='Полное описание'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='block1',
            name='text',
            field=models.CharField(max_length=5555, verbose_name='Текст'),
        ),
    ]
