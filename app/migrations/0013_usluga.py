# Generated by Django 3.1.3 on 2021-01-19 08:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0012_auto_20210113_1551'),
    ]

    operations = [
        migrations.CreateModel(
            name='Usluga',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=255, null=True, verbose_name='Заголовок')),
                ('baner', models.ImageField(blank=True, null=True, upload_to='', verbose_name='Банер БОЛЬШОЙ')),
                ('text', models.TextField(max_length=955155, verbose_name='Полное описание')),
            ],
        ),
    ]
